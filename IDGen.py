# %% Packages
from PyQt5 import uic
from PyQt5.QtSql import QSqlQueryModel, QSqlDatabase
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import Qt, QSortFilterProxyModel
from time import strftime
from datetime import datetime
import win32com.client as win32
import openpyxl
import pandas
import sqlite3
import shutil
import os


global con, cursor, boqfile, book, mapold, mapnew, cprilist, targetfileabs, generationmode, upeua, \
    oldbbutype, newbbutype, xl

rruremark = ''
matchedones = QSqlQueryModel()
totallymatchedones = QSqlQueryModel()
totallymatchedparts = QSqlQueryModel()
finallyselectednename = ''
u2k = '0'
db = QSqlDatabase.addDatabase('QSQLITE')
generrors = []

# RUN UI
Form, Window = uic.loadUiType("IDGenUI.ui")
app = QApplication([])
window = Window()
form = Form()
form.setupUi(window)
window.show()


def create_db_if_not_exist():

    global con, cursor

    cursor.execute("PRAGMA main.auto_vacuum = 1")

    cursor.execute("CREATE TABLE IF NOT EXISTS rruraw("
                   "id integer PRIMARY KEY, "
                   "NEName text, "
                   "Subrack integer,"
                   "RRUChain integer, "
                   "RRUPos integer, "
                   "RRUT text, "
                   "RRUWM text, "
                   "HeadSlot integer, "
                   "HeadPort integer, "
                   "antiduplicate text)")

    cursor.execute("CREATE TABLE IF NOT EXISTS rru("
                   "id integer PRIMARY KEY, "
                   "NEName text, "
                   "Subrack integer,"
                   "RRUChain integer, "
                   "RRUPos integer, "
                   "RRUT text, "
                   "RRUWM text, "
                   "HeadSlot integer, "
                   "HeadPort integer)")

    cursor.execute("CREATE TABLE IF NOT EXISTS rruchain("
                   "id integer PRIMARY KEY, "
                   "NEName text, "
                   "RRUChain integer, "
                   "HeadSlot integer, "
                   "HeadPort integer)")

    cursor.execute("CREATE TABLE IF NOT EXISTS rruchainraw("
                   "id integer PRIMARY KEY, "
                   "NEName text, "
                   "RRUChain integer, "
                   "HeadSlot integer, "
                   "HeadPort integer, "
                   "antiduplicate text)")

    cursor.execute("CREATE TABLE IF NOT EXISTS subrack("
                   "id integer PRIMARY KEY, "
                   "NEName text, "
                   "Subrack text, "
                   "FrameType text)")

    cursor.execute("CREATE TABLE IF NOT EXISTS board("
                   "id integer PRIMARY KEY, "
                   "NEName text, "
                   "Subrack text, "
                   "Slot text, "
                   "BoardName text, "
                   "RRUT text, "
                   "BBUT text, "
                   "BRDT text)")

    cursor.execute("CREATE TABLE IF NOT EXISTS matching("
                   "OSSNEName text, "
                   "CustomerNEID text, "
                   "LongCustomerNEID text, "
                   "IDVKFile text, "
                   "IDVKNEName text, "
                   "MasterFlag integer, "
                   "Slot6slave text, "
                   "Slot7slave text)")

    cursor.execute("CREATE TABLE IF NOT EXISTS boqbynes("
                   "OSSNEName text, "
                   "BOQFile text, "
                   "FirstBOQRow integer, "
                   "LastBOQRow integer, "
                   "FirstExtRow integer, "
                   "LastExtRow integer)")

    cursor.execute("CREATE TABLE IF NOT EXISTS boqbyparts("
                   "OSSNEName text, "
                   "PartType text, "
                   "PartName text, "
                   "NeedTo text, "
                   "PartNumber text, "
                   "PartModel text, "
                   "PartDescription text, "
                   "PartQty integer)")

    cursor.execute("CREATE VIEW IF NOT EXISTS bsname AS SELECT subrack.NEName "
                   "FROM subrack WHERE subrack.Subrack = 0 ORDER BY NEName")

    cursor.execute("CREATE VIEW IF NOT EXISTS mflagandid AS "
                   "SELECT OSSNEName as NEName, CustomerNEID as NEID, MasterFlag FROM matching")
    con.commit()


def fillidvkcombobox():
    global u2k
    dirlist = []
    for something in os.listdir('.\\' + u2k + '\\IDVK'):
        if os.path.isdir('.\\' + u2k + '\\IDVK\\' + something):
            dirlist.append(something)
    form.comboBox.clear()
    form.comboBox.addItems(dirlist)


def switchto11():
    global db, con, cursor, u2k
    u2k = '11'
    form.label_6.setText('Current server: 11')
    try:
        con.close()
    except:
        pass
    con = sqlite3.connect('11\\bs.db')
    cursor = con.cursor()
    create_db_if_not_exist()
    db.close()
    db.setDatabaseName("11\\bs.db")
    db.open()
    fillbssearchinglists()
    fillidvkcombobox()


def switchto12():
    global db, con, cursor, u2k
    u2k = '12'
    form.label_6.setText('Current server: 12')
    try:
        con.close()
    except:
        pass
    con = sqlite3.connect('12\\bs.db')
    cursor = con.cursor()
    create_db_if_not_exist()
    db.close()
    db.setDatabaseName("12\\bs.db")
    db.open()
    fillbssearchinglists()
    fillidvkcombobox()


def switchto13():
    global db, con, cursor, u2k
    u2k = '13'
    form.label_6.setText('Current server: 13')
    try:
        con.close()
    except:
        pass
    con = sqlite3.connect('13\\bs.db')
    cursor = con.cursor()
    create_db_if_not_exist()
    db.close()
    db.setDatabaseName("13\\bs.db")
    db.open()
    fillbssearchinglists()
    fillidvkcombobox()


def switchto14():
    global db, con, cursor, u2k
    u2k = '14'
    form.label_6.setText('Current server: 14')
    try:
        con.close()
    except:
        pass
    con = sqlite3.connect('14\\bs.db')
    cursor = con.cursor()
    create_db_if_not_exist()
    db.close()
    db.setDatabaseName("14\\bs.db")
    db.open()
    fillbssearchinglists()
    fillidvkcombobox()


def rrutypedetection(row):
    rrutype = ''
    if row['BoardName'] in ['MRRU', 'LRRU', 'WRRU', 'GRRU', 'MRFU', 'LRFU', 'WRFU', 'GRFU', 'DRRU', 'DRHX', 'AIRU']:
        for ManDataSubitem in row['ManData'].split(','):
            if ManDataSubitem[:3] in ['RRU', 'DRH', 'AAU']:
                rrutype = ManDataSubitem
        if rrutype == '':
            rrutype = row['BoardName']
        return rrutype
    else:
        return pandas.NA


def brdtypedetection(row):
    btype = pandas.NA
    if row['BoardName'] in ['UBBP', 'WBBP']:
        btype = str(''.join(list(row['BoardType'])[-6:]))
    elif row['BoardName'] in ['UMPT', 'UPEU', 'GTMU', 'UBRI', 'UTRP', 'LMPT', 'LBBP']:
        btype = str(''.join(list(row['BoardType'])[4:]))
    elif row['BoardName'] == 'FAN':
        btype = str(''.join(list(row['BoardType'])[5:]))
    elif row['BoardName'] == 'UEIU':
        if len(list(row['BoardType'])) > 5:
            if list(row['BoardType'])[5] == 'U':
                btype = ''.join(list(row['BoardType'])[5:])
            elif list(row['BoardType'])[4] == 'U':
                btype = ''.join(list(row['BoardType'])[4:])
    return btype


def clear_nedata_tables():
    global con, cursor
    cursor.execute('DELETE FROM rruchain')
    cursor.execute('DELETE FROM rruchainraw')
    cursor.execute('DELETE FROM rruraw')
    cursor.execute('DELETE FROM rru')
    cursor.execute('DELETE FROM subrack')
    cursor.execute('DELETE FROM board')
    cursor.execute('DELETE FROM matching')
    cursor.execute('DELETE FROM boqbynes')
    cursor.execute('DELETE FROM boqbyparts')
    con.commit()
    cursor.execute("VACUUM")


def run_import_now():
    global con, cursor
    clear_nedata_tables()

    print(strftime("\n %H:%M:%S"), 'Exporting is started...')

    book = 0
    sheet = 0
    errors = []
    rrufiles = []
    boardfiles = []
    subrackfiles = []

    # Scan BRD files
    for something in os.listdir('.\\' + u2k + '\\BRD'):
        if len(something.split('.')) > 1 and something.split('.')[-1] == 'csv':
            if something.split('_')[0] == 'Inventory':
                if something.split('_')[1] == 'Subrack':
                    subrackfiles.append(something)
                elif something.split('_')[1] == 'Board':
                    boardfiles.append(something)
    if len(subrackfiles) == 0:
        print('\n', strftime(" %H:%M:%S"),
              'No subrack inventory files found in "../BRD" folder. (Inventory_Subrack_*.csv)\n Operation failed.')
    if len(boardfiles) == 0:
        print('\n', strftime(" %H:%M:%S"),
              'No board inventory files found in "../BRD" folder. (Inventory_Board_*.csv)\n Operation failed.')

    # Scan RRU files
    for something in os.listdir('.\\' + u2k + '\\RRU'):
        if len(something.split('.')) > 1 and something.split('.')[-1] == 'xlsx':
            rrufiles.append(something)
    if len(rrufiles) == 0:
        print('\n', strftime(" %H:%M:%S"),
              'No XLSX files found in "../RRU" folder. \n Operation failed.')

    # Subrack export
    for file in subrackfiles:
        print(strftime(" %H:%M:%S"),
              'File "..BRD\\' + str(file) + '" is exporting... ', end="\r")
        sheet = pandas.read_csv('.\\' + u2k + '\\BRD\\' + file,
                                header=0, usecols=['NEName', 'Subrack No.', 'Frame Type'])
        sheet.rename(columns={'Subrack No.': 'Subrack', 'Frame Type': 'FrameType'}, inplace=True)
        sheet.to_sql('subrack', con=con, index=False, if_exists='append')
        con.commit()
        print(strftime(" %H:%M:%S"),
              'File "..BRD\\' + str(file) + '" is done.                                ')

    # Board export
    for file in boardfiles:
        print(strftime(" %H:%M:%S"),
              'File "..BRD\\' + str(file) + '" is exporting... ', end="\r")
        sheet = pandas.read_csv('.\\' + u2k + '\\BRD\\' + file, header=0,
                                usecols=['NEName', 'Subrack No.', 'Slot No.',
                                         'Board Name', 'Board Type', 'Manufacturer Data'],
                                dtype={2: "string"})
        sheet.rename(columns={'Subrack No.': 'Subrack',
                              'Slot No.': 'Slot',
                              'Board Name': 'BoardName',
                              'Board Type': 'BoardType',
                              'Manufacturer Data': 'ManData'},
                     inplace=True)
        sheet = sheet.loc[sheet['BoardName'] != 'FModule']
        sheet['ManData'] = sheet['ManData'].str.replace('-', ',')
        sheet['ManData'] = sheet['ManData'].str.replace(' ', ',')
        sheet['RRUT'] = sheet.apply(rrutypedetection, axis=1)
        sheet['BRDT'] = sheet.apply(brdtypedetection, axis=1)
        sheet = sheet.drop(['ManData'], axis=1)
        sheet = sheet.drop(['BoardType'], axis=1)
        sheet.to_sql('board', con=con, index=False, if_exists='append')
        con.commit()
        print(strftime(" %H:%M:%S"),
              'File "..BRD\\' + str(file) + '" is done.                                 ')

    # RRU data export
    for file in rrufiles:
        try:
            book = openpyxl.open('.\\' + u2k + '\\RRU\\' + str(file), read_only=True)
        except:
            print(strftime(" %H:%M:%S"),
                  'Book "..RRU\\' + str(file) + '" open failed')
            errors.append(' Book "..RRU\\' + str(file) + '" open failed')
        else:
            if 'RRU' in book.sheetnames:
                # %% RRU sheet export
                try:
                    sheet = book['RRU']
                except:
                    print(strftime(" %H:%M:%S"),
                          'The page "RRU" in book "..RRU\\' + str(file) + '" doesn\'t exist')
                    errors.append(' The page "RRU" in book "..RRU\\' + str(file) + '" doesn\'t exist')
                else:
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "RRU" is exporting... ', end="\r")
                    for row in sheet.iter_rows(min_row=3, values_only=True):
                        cursor.execute("INSERT INTO rruraw(NEName, Subrack, RRUChain, RRUPos, RRUWM, antiduplicate) "
                                       "VALUES(?, ?, ?, ?, ?, ?)",
                                       (str(row[0]),
                                        str(row[2]),
                                        str(row[5]),
                                        str(row[6]),
                                        str(row[13]),
                                        str(row[0]) + str(row[2])))
                    con.commit()
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "RRU" is done.                                  ')
            else:
                # BTSRXUCHAIN sheet export (for GBTS)
                try:
                    sheet = book['BTSRXUBRD']
                except:
                    print(strftime(" %H:%M:%S"),
                          'There is no page "RRU" or "BTSRXUBRD" in book "..RRU\\' + str(file) + '"')
                    errors.append(' There is no page "RRU" or "BTSRXUBRD" in book "..RRU\\' + str(file) + '"')
                else:
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "BTSRXUBRD" is exporting... ', end="\r")
                    for row in sheet.iter_rows(min_row=3, values_only=True):
                        cursor.execute("INSERT INTO rruraw(NEName, Subrack, RRUChain, RRUPos, RRUWM, antiduplicate) "
                                       "VALUES(?, ?, ?, ?, ?, ?)",
                                       (str(row[1]),
                                        str(row[7]),
                                        str(row[10]),
                                        str(row[11]),
                                        'GO',
                                        str(row[1]) + str(row[7])))
                    con.commit()
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "BTSRXUBRD" is done.                            ')
            book.close()
    cursor.execute("INSERT INTO rru "
                   "SELECT DISTINCT id, NEName, Subrack, RRUChain, RRUPos, RRUT, RRUWM, HeadSlot, HeadPort "
                   "FROM rruraw GROUP BY antiduplicate")
    cursor.execute('DELETE FROM rruraw')
    con.commit()
    print(strftime(" %H:%M:%S"), 'RRU antiduplicate operation complete.')

    # RRUCHAIN data export
    for file in rrufiles:
        try:
            book = openpyxl.open('.\\' + u2k + '\\RRU\\' + str(file), read_only=True)
        except:
            print(strftime(" %H:%M:%S"),
                  'Book "..RRU\\' + str(file) + '" open failed')
            errors.append(' Book "..RRU\\' + str(file) + '" open failed')
        else:
            if 'RRUCHAIN' in book.sheetnames:
                try:
                    sheet = book['RRUCHAIN']
                except:
                    print(strftime(" %H:%M:%S"),
                          'The page "RRUCHAIN" in book "..RRU\\' + str(file) + '" doesn\'t exist')
                    errors.append(' The page "RRUCHAIN" in book "..RRU\\' + str(file) + '" doesn\'t exist')
                else:
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "RRUCHAIN" is exporting... ', end="\r")
                    for row in sheet.iter_rows(min_row=3, values_only=True):
                        cursor.execute("INSERT INTO rruchainraw(NEName, RRUChain, HeadSlot, HeadPort, antiduplicate) "
                                       "VALUES(?, ?, ?, ?, ?)",
                                       (str(row[0]),
                                        str(row[1]),
                                        str(row[7]),
                                        str(row[8]),
                                        str(row[0]) + str(row[1])))
                    con.commit()
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "RRUCHAIN" is done.                            ')
            else:
                try:
                    sheet = book['BTSRXUCHAIN']
                except:
                    print(strftime(" %H:%M:%S"),
                          'There is no page "RRUCHAIN" or "BTSRXUCHAIN" in book "..RRU\\' + str(file) + '"')
                    errors.append(' There is no page "RRUCHAIN" or "BTSRXUCHAIN" in book "..RRU\\' + str(file) + '"')
                else:
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "BTSRXUCHAIN" is exporting... ', end="\r")
                    for row in sheet.iter_rows(min_row=3, values_only=True):
                        cursor.execute("INSERT INTO rruchainraw(NEName, RRUChain, HeadSlot, HeadPort, antiduplicate) "
                                       "VALUES(?, ?, ?, ?, ?)",
                                       (str(row[1]),
                                        str(row[2]),
                                        str(row[6]),
                                        str(row[7]),
                                        str(row[1]) + str(row[2])))
                    con.commit()
                    print(strftime(" %H:%M:%S"),
                          'Book "..RRU\\' + str(file) + '" - sheet "BTSRXUCHAIN" is done.                         ')
            book.close()
    cursor.execute("INSERT INTO rruchain "
                   "SELECT DISTINCT id, NEName, RRUChain, HeadSlot, HeadPort "
                   "FROM rruchainraw GROUP BY antiduplicate")
    cursor.execute('DELETE FROM rruchainraw')
    con.commit()
    print(strftime(" %H:%M:%S"), 'RRUCHAIN antiduplicate operation complete.')
    cursor.execute("VACUUM")
    print(strftime(" %H:%M:%S"), 'Boards and RRU import complete.')
    clickautocompletematchingdirectly()

    # Report
    if len(errors) != 0:
        print('\n Total errors: ' + str(len(errors)))
        for error in errors:
            print('  - ' + error)

    # SQLing
    print(strftime(" %H:%M:%S"), 'SQLing has begun, it must take less than 15 minutes.\n',
          strftime("%H:%M:%S"), 'Head slots are mapping...', end="\r")
    cursor.execute("UPDATE rru SET HeadSlot = (SELECT rruchain.HeadSlot FROM rruchain "
                   "WHERE rruchain.NEName = rru.NEName AND rruchain.RRUChain = rru.RRUChain)")
    con.commit()
    print(strftime(" %H:%M:%S"), 'Head slots are done.                                \n',
          strftime("%H:%M:%S"), 'Head ports are mapping...', end="\r")
    cursor.execute("UPDATE rru SET HeadPort = (SELECT rruchain.HeadPort FROM rruchain "
                   "WHERE rruchain.NEName = rru.NEName AND rruchain.RRUChain = rru.RRUChain)")
    con.commit()
    print(strftime(" %H:%M:%S"), 'Head ports are done.                                \n',
          strftime("%H:%M:%S"), 'RRU types are mapping...', end="\r")
    cursor.execute("UPDATE rru SET RRUT = (SELECT board.RRUT FROM board "
                   "WHERE board.NEName = rru.NEName AND board.Subrack = rru.Subrack AND rru.Subrack > 40)")
    con.commit()
    print(strftime(" %H:%M:%S"), 'RRU types are done.                                 \n',
          strftime("%H:%M:%S"), 'BBU types are mapping...', end="\r")
    cursor.execute("UPDATE board SET BBUT = (SELECT subrack.FrameType FROM subrack "
                   "WHERE subrack.Subrack = 0 AND board.Subrack = 0 AND subrack.NEName = board.NEName "
                   "AND (board.Slot = 6 OR board.Slot = 7))")
    con.commit()
    cursor.execute("UPDATE board SET BRDT = BoardName WHERE BRDT ISNULL")
    print(strftime(" %H:%M:%S"), 'BBU types are done.                                 \n',
          strftime("%H:%M:%S"), 'MBTS items integration started...', end="\r")
    cursor.execute("UPDATE matching SET MasterFlag = 1 WHERE (SELECT count(NEID) FROM mflagandid "
                   "WHERE mflagandid.NEID = matching.CustomerNEID) = 1")
    con.commit()
    cursor.execute("UPDATE matching SET MasterFlag = 2 WHERE (SELECT count(NEID) FROM mflagandid "
                   "WHERE mflagandid.NEID = matching.CustomerNEID) > 1 AND OSSNEName like 'MBTS%'")
    con.commit()
    mbtslist = cursor.execute("SELECT NEName, NEID FROM mflagandid WHERE MasterFlag = 2").fetchall()
    loopend = len(mbtslist)
    step = 0
    for mbts in mbtslist:
        slavecandidates = cursor.execute("SELECT OSSNEName FROM matching WHERE MasterFlag ISNULL AND CustomerNEID = ?",
                                         (mbts[1],)).fetchall()
        mastercontent = cursor.execute("SELECT BRDT, Slot, BBUT FROM board WHERE NEName = ?",
                                         (mbts[0],)).fetchall()
        for slave in slavecandidates:
            slavecontent = cursor.execute("SELECT BRDT, Slot, BBUT FROM board WHERE NEName = ?",
                                          (slave[0],)).fetchall()
            if set(slavecontent).issubset(set(mastercontent)):
                for board in slavecontent:
                    if board[2] != '':
                        if board[1] == '6':
                            cursor.execute("UPDATE matching SET Slot6slave = ? WHERE OSSNEName = ?",
                                           (slave[0], mbts[0]))
                            break
                        elif board[1] == '7':
                            cursor.execute("UPDATE matching SET Slot7slave = ? WHERE OSSNEName = ?",
                                           (slave[0], mbts[0]))
                            break
        step += 1
        print(f' Merged [{step}/{loopend}]', mbts[0], '                                                     ', end="\r")
    print(strftime(" %H:%M:%S"), 'MBTS items integration complete.                                                  \n',
          strftime("%H:%M:%S"), 'RRU binding to MBTS started...', end="\r")
    cursor.execute("UPDATE rru SET NEName = matching.OSSNEName FROM matching "
                   "WHERE matching.Slot6slave = rru.NEName OR matching.Slot7slave = rru.NEName")
    print(strftime(" %H:%M:%S"), 'RRU binding to MBTS complete.             \n SQLing complete.\n Use a tool, be cool.')
    con.commit()
    cursor.execute("VACUUM")


def valuesfromfile():
    global con, cursor
    cursor.execute('DELETE FROM matching')

    try:
        book = openpyxl.open('matching.xlsx', read_only=True)
    except:
        print('\n File "matching.xlsx" open failed.')
    else:
        sheet = book.active
        print('\n Matching table is importing from "matching.xlsx"... ')
        for row in sheet.iter_rows(min_row=2):
            cursor.execute("INSERT INTO matching(OSSNEName, CustomerNEID) "
                           "VALUES(?, ?)", (row[0].value, row[1].value))
        con.commit()
        print(' Matching table complete.')
        book.close()


def clickautocompletematchingdirectly():
    global con, cursor
    cursor.execute('DELETE FROM matching')
    bsnames = cursor.execute("SELECT * FROM bsname").fetchall()

    for bsname in bsnames:
        bsid = []
        for symbol in list(bsname[0]):
            if symbol.isdigit():
                bsid.append(symbol)
            else:
                if len(bsid) != 0:
                    break
        cursor.execute("INSERT INTO matching(OSSNEName, CustomerNEID) VALUES(?, ?)", (bsname[0], ''.join(bsid)))
    con.commit()
    print(strftime(" %H:%M:%S"), 'Short ID autocompletion done.')


def autocompletefile():
    global con, cursor
    cursor.execute('DELETE FROM matching')

    try:
        book = openpyxl.open('matching.xlsx')
    except:
        print('\n File "matching.xlsx" open failed.')
    else:
        sheet = book.active
        print('\n File "matching.xlsx" is under autocompleting...')
        cursor.execute("SELECT * FROM bsname")
        bsnames = cursor.fetchall()
        for i in range(0, len(bsnames)):
            sheet.cell(row=i+2, column=1, value=bsnames[i][0])
            customerbsname = []
            for letter in list(bsnames[i][0]):
                if letter.isdigit():
                    customerbsname.append(letter)
                else:
                    if len(customerbsname) != 0:
                        break
            sheet.cell(row=i + 2, column=2, value=''.join(customerbsname))
        book.save(filename='matching.xlsx')
        print(' File "matching.xlsx" autocomplete is done.\n')
        book.close()


def longvaluesfromfile():
    global con, cursor

    try:
        book = openpyxl.open('shortlong.xlsx', read_only=True)
    except:
        print('\n File "shortlong.xlsx" open failed.')
    else:
        sheet = book.active
        print('\n Long customer BS ID matching from "shortlong.xlsx" is in progress... ')
        for row in sheet.iter_rows(min_row=2):
            cursor.execute("UPDATE matching SET LongCustomerNEID = ? WHERE CustomerNEID = ?",
                           (row[1].value, row[0].value))

        con.commit()
        matchescount = cursor.execute("SELECT LongCustomerNEID FROM matching WHERE LongCustomerNEID != ''").fetchall()
        print(' Long customer BS ID matching is complete. '
              '\n Total: ' + str(len(matchescount)) + ' matches.')
        book.close()


# BS for Searchers
def matchedsearchchanged(self):
    matchedones.setQuery("SELECT OSSNEName AS 'OSS NE name', IDVKNEName AS 'IDVK name', CustomerNEID AS 'Short NE ID', "
                         "LongCustomerNEID AS 'Long NE ID' FROM matching "
                         "WHERE IDVKFile != '' AND MasterFlag > 0 AND OSSNEName LIKE '%{0}%'".format(self))


def totallymatchedsearchchanged(self):
    totallymatchedones.setQuery("SELECT DISTINCT "
                                "matching.OSSNEName AS 'OSS NE name', matching.IDVKNEName AS 'IDVK NE name', "
                                "matching.CustomerNEID AS 'Short NE ID', matching.LongCustomerNEID AS 'Long NE ID' "
                                # "matching.IDVKFile AS 'IDVK', boqbynes.BOQFile AS 'BOQ', "
                                # "boqbynes.FirstBOQRow, boqbynes.LastBOQRow, "
                                # "boqbynes.FirstExtRow, boqbynes.LastExtRow "
                                "FROM matching, boqbynes "
                                "WHERE IDVKFile != '' "
                                "AND  matching.OSSNEName = boqbynes.OSSNEName "
                                "AND boqbynes.BOQfile != '' "
                                "AND matching.MasterFlag > 0 "
                                "AND matching.OSSNEName LIKE '%{0}%'"
                                "UNION "
                                "SELECT DISTINCT "
                                "matching.OSSNEName AS 'OSS NE name', matching.IDVKNEName AS 'IDVK NE name', "
                                "matching.CustomerNEID AS 'Short NE ID', matching.LongCustomerNEID AS 'Long NE ID' "
                                # "matching.IDVKFile AS 'IDVK', boqbynes.BOQFile AS 'BOQ', "
                                # "boqbynes.FirstBOQRow, boqbynes.LastBOQRow, "
                                # "boqbynes.FirstExtRow, boqbynes.LastExtRow "
                                "FROM matching, boqbynes "
                                "WHERE IDVKFile != '' "
                                "AND  matching.OSSNEName = boqbynes.OSSNEName "
                                "AND boqbynes.BOQfile != '' "
                                "AND matching.MasterFlag > 0 "
                                "AND matching.IDVKNEName LIKE '%{0}%'".format(self))


def fillbssearchinglists():
    global db, con, cursor
    # ALL BS searcher
    cursor.execute("SELECT OSSNEName FROM matching WHERE MasterFlag > 0")
    bsnames = cursor.fetchall()
    model_bsnames = QStandardItemModel(len(bsnames), 1)
    for row, bsname in enumerate(list(bsnames)):
        model_bsnames.setItem(row, 0, QStandardItem(bsname[0]))
    filter_bsnames = QSortFilterProxyModel()
    filter_bsnames.setSourceModel(model_bsnames)
    filter_bsnames.setFilterCaseSensitivity(Qt.CaseInsensitive)
    filter_bsnames.setFilterKeyColumn(0)
    form.lineEdit.textChanged.connect(filter_bsnames.setFilterRegExp)
    form.listView.setModel(filter_bsnames)
    form.tableView.setModel(None)
    form.tableView_2.setModel(None)
    form.tableView_5.setModel(None)

    # Matched by IDVK
    form.lineEdit_2.textChanged.connect(matchedsearchchanged)
    matchedones.setQuery("SELECT OSSNEName AS 'OSS NE name', IDVKNEName AS 'IDVK name', CustomerNEID AS 'Short NE ID', "
                         "LongCustomerNEID AS 'Long NE ID' FROM matching "
                         "WHERE IDVKFile != '' AND MasterFlag > 0")
    form.tableView_4.setModel(matchedones)
    form.tableView_4.resizeColumnsToContents()

    # Fully matched searcher
    form.lineEdit_3.textChanged.connect(totallymatchedsearchchanged)
    totallymatchedones.setQuery("SELECT DISTINCT "
                                "matching.OSSNEName AS 'OSS NE name', matching.IDVKNEName AS 'IDVK NE name', "
                                "matching.CustomerNEID AS 'Short NE ID', matching.LongCustomerNEID AS 'Long NE ID' "
                                # "matching.IDVKFile AS 'IDVK', boqbynes.BOQFile AS 'BOQ', "
                                # "boqbynes.FirstBOQRow, boqbynes.LastBOQRow, boqbynes.FirstExtRow, boqbynes.LastExtRow "
                                "FROM matching, boqbynes "
                                "WHERE IDVKFile != '' AND  matching.OSSNEName = boqbynes.OSSNEName "
                                "AND boqbynes.BOQfile != '' AND matching.MasterFlag > 0")

    form.tableView_3.setModel(totallymatchedones)
    form.tableView_3.resizeColumnsToContents()


# Fill selected BS content
def fillselectedbsbrd():
    global cursor
    boards = cursor.fetchall()
    if len(boards) != 0:
        model_boards = QStandardItemModel(len(boards), 4)
        model_boards.setHorizontalHeaderLabels(['Board', 'Type', 'Slot', 'BBU'])
        for i, boardtuple in enumerate(list(boards)):
            model_boards.setItem(i, 0, QStandardItem(boardtuple[0]))
            model_boards.setItem(i, 1, QStandardItem(str(boardtuple[1])))
            model_boards.setItem(i, 2, QStandardItem(boardtuple[2]))
            model_boards.setItem(i, 3, QStandardItem(boardtuple[3]))
            form.tableView.setModel(model_boards)
    else:
        form.tableView.setModel(None)


def fillselectedbsrru():
    global cursor
    rrus = cursor.fetchall()
    if len(rrus) != 0:
        model_rrus = QStandardItemModel(len(rrus), 7)
        model_rrus.setHorizontalHeaderLabels(['RRU', 'Mode', 'Subrack', 'Chain', 'Slot', 'Port', 'Position'])
        for i, rrutuple in enumerate(list(rrus)):
            model_rrus.setItem(i, 0, QStandardItem(rrutuple[0]))
            model_rrus.setItem(i, 1, QStandardItem(rrutuple[1]))
            model_rrus.setItem(i, 2, QStandardItem(str(rrutuple[2])))
            model_rrus.setItem(i, 3, QStandardItem(str(rrutuple[3])))
            model_rrus.setItem(i, 4, QStandardItem(str(rrutuple[4])))
            model_rrus.setItem(i, 5, QStandardItem(str(rrutuple[5])))
            model_rrus.setItem(i, 6, QStandardItem(str(rrutuple[6])))
            form.tableView_2.setModel(model_rrus)
    else:
        model_rrus = QStandardItemModel(1, 1)
        model_rrus.setHorizontalHeaderLabels(['No RRU in database'])
        form.tableView_2.setModel(model_rrus)


# BS click functions
def clickbslist(index):
    cursor.execute("SELECT board.BoardName, board.BRDT, board.Slot, board.BBUT "
                   "FROM board WHERE board.NEName = ? AND board.Subrack = 0 "
                   "ORDER BY Slot", (form.listView.model().data(index),))
    fillselectedbsbrd()

    cursor.execute("SELECT rru.RRUT, rru.RRUWM, rru.Subrack, rru.RRUChain, rru.HeadSlot, rru.HeadPort, rru.RRUPos "
                   "FROM rru WHERE rru.NEName = ? ORDER BY Subrack", (form.listView.model().data(index),))
    fillselectedbsrru()


def clickmatched(index):
    global finallyselectednename
    cursor.execute("SELECT board.BoardName, board.BRDT, board.Slot, board.BBUT "
                   "FROM board WHERE board.NEName = ? AND board.Subrack = 0 "
                   "ORDER BY Slot", (form.tableView_4.model().data(index, Qt.DisplayRole),))
    fillselectedbsbrd()

    cursor.execute("SELECT rru.RRUT, rru.RRUWM, rru.Subrack, rru.RRUChain, rru.HeadSlot, rru.HeadPort, rru.RRUPos "
                   "FROM rru WHERE rru.NEName = ? ORDER BY Subrack",
                   (form.tableView_4.model().data(index, Qt.DisplayRole),))
    fillselectedbsrru()


def clicktotallymatched(index):
    global finallyselectednename
    cursor.execute("SELECT board.BoardName, board.BRDT, board.Slot, board.BBUT "
                   "FROM board WHERE board.NEName = ? AND board.Subrack = 0 "
                   "ORDER BY Slot", (form.tableView_3.model().data(index, Qt.DisplayRole),))
    fillselectedbsbrd()

    cursor.execute("SELECT rru.RRUT, rru.RRUWM, rru.Subrack, rru.RRUChain, rru.HeadSlot, rru.HeadPort, rru.RRUPos "
                   "FROM rru WHERE rru.NEName = ? ORDER BY Subrack",
                   (form.tableView_3.model().data(index, Qt.DisplayRole),))
    fillselectedbsrru()
    totallymatchedparts.setQuery("SELECT boqbyparts.PartQty AS Quantity, boqbyparts.PartName AS Type, "
                                 "boqbyparts.PartDescription AS Description FROM boqbyparts "
                                 "WHERE OSSNEName = '{0}'".format(form.tableView_3.model().data(index,
                                                                  Qt.DisplayRole), ))
    form.tableView_5.setModel(totallymatchedparts)
    form.tableView_5.resizeColumnsToContents()
    finallyselectednename = format(form.tableView_3.model().data(index, Qt.DisplayRole), )


def clickrunbrdrrureimport():
    global con, cursor
    clear_nedata_tables()
    run_import_now()
    fillbssearchinglists()
    form.tableView.setModel(None)
    form.tableView_2.setModel(None)


def clickimportmatching():
    valuesfromfile()


def clickautocompletematching():
    autocompletefile()


def clickimportlongbsid():
    longvaluesfromfile()


def clickreconnectbsdb():
    global con, cursor
    pass


def clickxlstoxml():
    rawfilesendless = []
    for dirpath, dirnames, filenames in os.walk(".\\IDVKraw"):
        for filename in filenames:
            if filename[-4:] == '.xls':
                rawfilesendless.append(str(''.join(list(os.path.join(dirpath, filename))[10:-4])))
        for dirname in dirnames:
            if not os.path.exists('IDVK\\' + str(''.join(list(dirpath)[10:])) + '\\' + dirname):
                try:
                    os.mkdir('IDVK\\' + str(''.join(list(dirpath)[10:])) + '\\' + dirname)
                except:
                    print('failed to make directory: ' + 'IDVK\\' + str(''.join(list(dirpath)[10:])) + '\\' + dirname)
                else:
                    print(' New folder created: ' + 'IDVK' + str(''.join(list(dirpath)[10:])) + '\\' + dirname)

    idvkfilesendless = []
    for dirpath, dirnames, filenames in os.walk(".\\IDVK"):
        for filename in filenames:
            if filename[-4:] in ['xlsx', '.xml']:
                if filename[-4:] == 'xlsx':
                    idvkfilesendless.append(str(''.join(list(os.path.join(dirpath, filename))[7:-5])))
                    if os.path.isfile(str(''.join(list(os.path.join(dirpath, filename))[:-5])) + '.xml'):
                        try:
                            os.remove(str(''.join(list(os.path.join(dirpath, filename))[:-5])) + '.xml')
                        except:
                            pass
                        else:
                            print(' xml which already has xlsx example was deleted' +
                                  str(''.join(list(os.path.join(dirpath, filename))[:-5])) + '.xml')
                else:
                    idvkfilesendless.append(str(''.join(list(os.path.join(dirpath, filename))[7:-4])))

    newfilesendless = set(rawfilesendless) - set(idvkfilesendless)
    for newfile in newfilesendless:
        shutil.copy('IDVKraw\\' + newfile + '.xls', 'IDVK\\' + newfile + '.xml')

    print('New xml files:')
    for file in newfilesendless:
        print(' ', file + '.xml')
    print('\n Copy from IDVKraw to IDVK with renaming is finished.')


def listxlsxfilesinto(directory):
    global con
    global cursor
    thelist = []
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if str(''.join(list(os.path.join(dirpath, filename))[-5:])) == '.xlsx':
                thelist.append(str(os.path.join(dirpath, filename)))
    return thelist


def clickreloadmatchingidvk():
    global con, cursor

    # Broken XLS files healing
    idvkxlslist = []
    for dirpath, dirnames, filenames in os.walk('.\\' + u2k + '\\IDVK'):
        for filename in filenames:
            if filename[-4:] in ['.xls', '.XLS']:
                idvkxlslist.append(os.path.abspath(str(os.path.join(dirpath, filename))))
    try:
        excel = win32.gencache.EnsureDispatch('Excel.Application')
    except AttributeError:
        # Corner case dependencies.
        import re
        import sys
        # Remove cache and try again.
        module_list = [m.__name__ for m in sys.modules.values()]
        for module in module_list:
            if re.match(r'win32com\.gen_py\..+', module):
                del sys.modules[module]
        shutil.rmtree(os.path.join(os.environ.get('LOCALAPPDATA'), 'Temp', 'gen_py'))
        excel = win32.gencache.EnsureDispatch('Excel.Application')
    for idvkxls in idvkxlslist:
        wb = excel.Workbooks.Open(idvkxls)
        wb.SaveAs(idvkxls[:-4] + '.xlsx', FileFormat=51)  # FileFormat = 51 is for .xlsx extension
        print(f' Resaving broken XLS to XLSX [{idvkxls}]                                                    ', end="\r")
        wb.Close()
        os.remove(idvkxls)
    print(f' {len(idvkxlslist)} broken XLS fixed.')

    for file in listxlsxfilesinto(".\\" + u2k + "\\IDVK"):
        try:
            book = openpyxl.open(file, read_only=True)
        except:
            print(' File can\'t be opened: ' + file)
        else:
            sheet = book.active
            if cursor.execute("SELECT CustomerNEID FROM matching WHERE CustomerNEID = ?",
                              (sheet.cell(row=2, column=3).value,)).fetchone() is not None:
                cursor.execute("UPDATE matching SET IDVKFile = ?, IDVKNEName = ?, LongCustomerNEID = ? "
                               "WHERE CustomerNEID = ?",
                               (file, sheet.cell(row=6, column=3).value, sheet.cell(row=4, column=3).value,
                                sheet.cell(row=2, column=3).value))
                print(' Matched: ', sheet.cell(row=2, column=3).value + '_' + sheet.cell(row=6, column=3).value)
            else:
                cursor.execute("INSERT INTO matching(OSSNEName, CustomerNEID, LongCustomerNEID, "
                               "IDVKFile, IDVKNEName, MasterFlag) "
                               "VALUES(?, ?, ?, ?, ?, ?)",
                               (sheet.cell(row=2, column=3).value + '_' + sheet.cell(row=6, column=3).value,
                                sheet.cell(row=2, column=3).value,
                                sheet.cell(row=4, column=3).value, file,
                                sheet.cell(row=6, column=3).value, 1))
                print(' Added: ', sheet.cell(row=2, column=3).value + '_' + sheet.cell(row=6, column=3).value)
            con.commit()
    con.execute("VACUUM")
    fillbssearchinglists()
    print('\n Operation finished.\n')


def clickboqreload():
    global boqfile, con, cursor, book
    cursor.execute('DELETE FROM boqbynes')
    cursor.execute('DELETE FROM boqbyparts')
    con.commit()

    errors = []
    for file in listxlsxfilesinto(".\\" + u2k + "\\BOQ"):
        boqfile = file
        try:
            book = openpyxl.open(file, read_only=True)
        except:
            print(strftime("%H:%M:%S"), 'BOQ file open failed. File: "' + str(file) + '"')
            errors.append(' BOQ file open failed. File: "' + str(file) + '"')
        else:
            try:
                sheet = book['SWAP']
            except:
                print(strftime("%H:%M:%S"),
                      'The page "SWP" open failed. Book: "' + str(file) + '"')
                errors.append(' The page "SWP" open failed. Book: "' + str(file) + '"')
            else:
                print(strftime("%H:%M:%S"),
                      'BOQ is under data exporting... File: "' + str(file) + '"')
                boqimport(sheet, 'SWAP')
            try:
                sheet = book['Extension']
            except:
                print(strftime("%H:%M:%S"),
                      'The page "EXT" doesn\'t exist. Book: "' + str(file) + '"')
                errors.append(' The page "EXT" doesn\'t exist. Book: "' + str(file) + '"')
            else:
                print(strftime("%H:%M:%S"),
                      'Extensions are under exporting... File: "' + str(file) + '"')
                boqimport(sheet, 'Extension')
            finally:
                book.close()

    print('\n BOQ data import complete.')
    # %% Error report
    if len(errors) != 0:
        print('\n Total errors: ' + str(len(errors)))
        for error in errors:
            print('  - ' + error)


def scanbsidinboq(sheet, firstrow):
    bsidlist = []

    # for i in range(9, 15):
    if len(str(sheet.cell(row=firstrow, column=13).value).replace(' ', '').split(',')[0]) == 11:
        bsidlist = str(sheet.cell(row=firstrow, column=13).value).replace(' ', '').split(',')
        # break
    if len(bsidlist) == 0:
        titlecell = sheet.cell(row=firstrow, column=5).value
        if list(titlecell)[0] == '=':
            titletext = sheet[str(sheet.cell(row=firstrow, column=5).value).replace('=', '')].value
            titlelist = titletext.replace(' ', ',').replace('_', ',').split(',')
            for item in titlelist:
                if len(list(item)) == 11 and item.isdigit():
                    bsidlist.append(item)
        else:
            titlelist = titlecell.replace(' ', ',').replace('_', ',').split(',')
            for item in titlelist:
                if len(list(item)) == 11 and item.isdigit():
                    bsidlist.append(item)

    return bsidlist


def boqimport(sheet, page):
    global boqfile
    global con
    global cursor

    rowid = 1
    firstrow = 0
    summary = []
    for row in sheet.iter_rows():
        if row[12].value is not None:
            if firstrow != 0:
                summary.append([firstrow, rowid - 1] + scanbsidinboq(sheet, firstrow))
                firstrow = rowid
            else:
                firstrow = rowid
        rowid += 1
    summary.append([firstrow, sheet.max_row] + scanbsidinboq(sheet, firstrow))

    # заплатка на приморцев
    # for i in range(1, len(summary)):
    #     if summary[i][2:] == summary[i - 1][2:] and summary[i][2:] != []:
    #         puzzled = False
    #         if len(summary) >= i - 1 + len(summary[i][2:]):
    #             for j in range(1, len(summary[i][2:])):
    #                 if set(summary[i - 1][2:]) == set(summary[i - 1 + j][2:]):
    #                     puzzled = True
    #                 else:
    #                     puzzled = False
    #                     break
    #             if puzzled:
    #                 sourceitem = summary[i - 2 + len(summary[i - 1][2:])][2:]
    #                 for k in range(0, len(summary[i - 1][2:])):
    #                     targetitem = summary[i - 1 + k][:2]
    #                     targetitem.append(sourceitem[k])
    #                     summary[i - 1 + k] = targetitem
    # for i in range(0, len(summary)):
    #     duplicatechecklist = []
    #     for item in summary[i + 1:]:
    #         duplicatechecklist.extend(item[2:])
    #         for bsid in summary[i][2:]:
    #             if bsid in duplicatechecklist:
    #                 summary[i].remove(bsid)

    matchedbs = []
    matchedextbs = []
    for item in summary:
        print(item)
        for bsid in item[2:]:
            try:
                ossnename = str(cursor.execute("SELECT OSSNEName FROM matching "
                                               "WHERE LongCustomerNEID = ?"
                                               "ORDER BY MasterFlag DESC ", (bsid,)).fetchone()[0])
            except:
                pass
            else:
                importboqbyparts(sheet, ossnename, item[0], item[1])
                if page == 'SWAP':
                    cursor.execute("INSERT INTO boqbynes(OSSNEName, BOQFile, FirstBOQRow, LastBOQRow) "
                                   "VALUES(?, ?, ?, ?)", (ossnename, boqfile, item[0], item[1]))
                    matchedbs.append([ossnename, item[0], item[1]])
                elif page == 'Extension':
                    cursor.execute("INSERT INTO boqbynes(OSSNEName, BOQFile, FirstExtRow, LastExtRow) "
                                   "VALUES(?, ?, ?, ?)", (ossnename, boqfile, item[0], item[1]))
                    matchedextbs.append([ossnename, item[0], item[1]])
    con.commit()
    print(f' Found BS (record amount: {len(matchedbs) + len(matchedextbs)}):')
    for bs in matchedbs:
        print(' first SWP row:', bs[1], ' last SWP row:', bs[2], ' BS name:', bs[0])
    for bs in matchedextbs:
        print(' first EXT row:', bs[1], ' last EXT row:', bs[2], ' BS name:', bs[0])


brdlib = [
    ['03059443', 'UBBPg1'],
    ['03022HEG', 'UBBPd1'],
    ['03022HEJ', 'UBBPd3'],
    ['03057154', 'UBBPe3'],
    ['03057155', 'UBBPe4'],
    ['03057253', 'UMPTe3'],
    ['03057273', 'UMPTb3'],
    ['03057305', 'UMPTe1'],
    ['03057306', 'UMPTe2'],
    ['03058736', 'UMPTg1'],
    ['03058541', 'UMPTg1'],
    ['03058738', 'UMPTg3'],
    ['03022ACJ', 'UBRIb'],
    ['03058707', 'UBBPg2a'],
    ['02310SFM', 'UPEUd'],
    ['01073052', 'Power Cabinet (APM30H)'],
    ['01075061', 'Power (ETP48200-C5E1)'],
    ['01072468', 'Power (ETP48200-C5B4)'],
    ['01074659-002', 'Power (DEU04D)'],
    ['01074357', 'Power (TP48200A)'],
    ['01071123', 'Power (TP48200B)'],
    ['01072711', 'Power (TP48300B)'],
    ['01074979', 'Power (ETP48400)'],
    ['01074766-001', 'Power (MTS9300A)'],
    ['01075030', 'Power (MTS9304A)'],
    ['02120731', 'DCDU12B'],
    ['02122430', 'DCDU16D'],
    ['02122720', 'DCDU16D'],
    ['02120472', 'DCDU03B'],
    ['02120618', 'DCDU11B'],
    ['02312NGM-001', 'DCDU (2000W)'],
    ['02231GJJ', 'EMUb'],
    ['02311VFF', 'BBU5900 Box'],
    ['02310VTE', 'BBU3910 Box']]

rrulib = [  # PartNumber, PartName, WorkingMode, Priority during CPRI assignment
    ['02312GEB', 'RRU5509t(700MHz/900MHz)', 'U', 3],
    ['02312SJM', 'RRU5519et(700MHz/900MHz)', 'U', 3],
    ['02312FYQ', 'RRU5309(LTE700)', 'L', 5],
    ['02310QDG', 'RRU3268(LTE800)', 'L', 5],
    ['02311LDE', 'RRU3268(LTE800)', 'L', 5],
    ['02312JKA', 'RRU5308(LTE800)', 'L', 5],
    ['02311TVV', 'RRU5309(LTE800)', 'L', 5],
    ['02310CJW', 'RRU3929(900MHz)', 'U', 3],
    ['02310MNS', 'RRU3936(900MHz)', 'U', 3],
    ['02310PYC', 'RRU3938(900MHz)', 'U', 3],
    ['02311HVE', 'RRU3953(900MHz)', 'U', 3],
    ['02311BPD', 'RRU3959(900MHz)', 'U', 3],
    ['02312BCY', 'RRU5905(900MHz)', 'U', 3],
    ['02311TBA', 'RRU5909(900MHz)', 'U', 3],
    ['02311TAY', 'RRU5909(900MHz)', 'U', 3],
    ['02232UBP', 'RRU5909(900MHz)', 'U', 3],
    ['02232UBN', 'RRU5909(900MHz)', 'U', 3],
    ['02310CJV', 'RRU3929(1800MHz)', 'U', 4],
    ['02310MNR', 'RRU3936(1800MHz)', 'U', 4],
    ['02310PWP', 'RRU3938(1800MHz)', 'U', 4],
    ['02310SSQ', 'RRU3939(1800MHz)', 'U', 4],
    ['02311APA', 'RRU3953(1800MHz)', 'U', 4],
    ['02311BYE', 'RRU3959(1800MHz)', 'U', 4],
    ['02311HKL', 'RRU3971(1800MHz)', 'U', 4],
    ['02311QMD', 'RRU5901(1800MHz)', 'U', 4],
    ['02311UWH', 'RRU5904(1800MHz)', 'U', 4],
    ['02311PRD', 'RRU5905(1800MHz)', 'U', 4],
    ['02311PPP', 'RRU5909(1800MHz)', 'U', 4],
    ['02232UBM', 'RRU5909(1800MHz)', 'U', 4],
    ['02311BBK', 'RRU3953w(1800MHz)', 'U', 4],
    ['02311BTE', 'RRU3959w(1800MHz)', 'U', 4],
    ['02312AAR', 'RRU5901w(1800MHz)', 'U', 4],
    ['02312EEM', 'RRU5904w(1800MHz)', 'U', 4],
    ['02311SJK', 'RRU5905w(1800MHz)', 'U', 4],
    ['02311VMD', 'RRU5501(1800MHz/2100MHz)', 'U', 2],
    ['02312BSJ', 'RRU5502(1800MHz/2100MHz)', 'U', 2],
    ['02312SAQ', 'RRU5505(1800MHz/2100MHz)', 'U', 2],
    ['02312FLG', 'RRU5505(1800MHz/2100MHz)', 'U', 2],
    ['02312BRX', 'RRU5508(1800MHz/2100MHz)', 'U', 2],
    ['02312FMR', 'RRU5502w(1800MHz/2100MHz)', 'U', 2],
    ['02312SJF', 'RRU5935E(1800MHz/2100MHz)', 'U', 2],
    ['02315353', 'RRU(WCDMA1800)', 'W', 1],
    ['02310UNY', 'RRU3824(2100MHz)', 'W', 1],
    ['02310KKQ', 'RRU3826(2100MHz)', 'W', 1],
    ['02310MAA', 'RRU3838(2100MHz)', 'W', 1],
    ['02311UWT', 'RRU5904(2100MHz)', 'W', 1],
    ['02311TBD', 'RRU5909(2100MHz)', 'W', 1],
    ['02310NVU', 'RRU3268(2600MHz)', 'L', 5],
    ['02231FMB', 'RRU3252(2600MHz)', 'L', 5],
    ['02231FME', 'RRU3256(2600MHz)', 'L', 5],
    ['02231KSS', 'RRU3259(2600MHz)', 'L', 5],
    ['02310NVV', 'RRU3260(2600MHz)', 'L', 5],
    ['02310WWD', 'RRU3262(2600MHz)', 'L', 5],
    ['02311KVB', 'RRU3268(2600MHz)', 'L', 5],
    ['02231UMB', 'RRU3276(2600MHz)', 'L', 5],
    ['02311LBM', 'RRU3276(2600MHz)', 'L', 5],
    ['02231UMG', 'RRU3279(2600MHz)', 'L', 5],
    ['02311HEF', 'RRU3281(2600MHz)', 'L', 5],
    ['02312QFS', 'RRU5258(2600MHz)', 'L', 5],
    ['02312GCA', 'RRU5258(2600MHz)', 'L', 5],
    ['02311PFF', 'RRU5301(2600MHz)', 'L', 5],
    ['RRU5336E', 'RRU5336E(2600MHz)', 'L', 5],
    ['02231FLW', 'RRU3232(3500MHz)', 'L', 5],
    ['02312AFL', 'RRU5258(3500MHz)', 'L', 5],
    ['nonumber', 'RRU3276(TDL)', 'U', 5]]


def importboqbyparts(sheet, ossnename, startrow, endrow):
    global rrulib, brdlib

    for row in sheet.iter_rows(min_row=startrow, max_col=7, max_row=endrow):
        partname = ''
        parttype = ''
        if row[5].value is not None:
            partnumber = str(row[3].value)

            # уборка пробелов в партномерах
            if partnumber[0] == ' ':
                while partnumber[0] != '':
                    partnumber = partnumber[1:]
            if partnumber[-1] == ' ':
                while partnumber[-1] != '':
                    partnumber = partnumber[:-1]

            for rru in rrulib:
                if partnumber in [rru[0], rru[0][1:]]:
                    partname = rru[1]
                    parttype = 'RRU'
                    break
                else:
                    for brd in brdlib:
                        if partnumber in [brd[0], brd[0][1:]]:
                            partname = brd[1]
                            parttype = 'BRD'
                            break

            cursor.execute("INSERT INTO boqbyparts(OSSNEName, PartType, PartName, NeedTo, "
                           "PartNumber, PartModel, PartDescription, PartQty) "
                           "VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                           (ossnename, parttype, partname, None,
                            row[3].value, row[4].value, row[5].value, row[6].value))
        con.commit()


def drawingboq(targetfilepath, mtuple):
    global cursor, xl
    # mtuple ==
    #           [matching.OSSNEName, matching.IDVKFile, boqbynes.BOQFile, boqbynes.FirstBOQRow, boqbynes.LastBOQRow,
    #            matching.CustomerNEID, boqbynes.FirstExtRow, boqbynes.LastExtRow]
    global book
    xl = win32.Dispatch("Excel.Application")
    xl.Visible = False
    wb2 = xl.Workbooks.Open(Filename=targetfilepath)
    ws2 = wb2.Sheets.Add(After=wb2.Worksheets(2))
    ws2.Name = 'BOQ'
    ws2.Columns(1).ColumnWidth = 30
    ws2.Columns(2).ColumnWidth = 15
    ws2.Columns(3).ColumnWidth = 20
    ws2.Columns(4).ColumnWidth = 70
    ws2.Columns(5).ColumnWidth = 10
    boqpapercuts = cursor.execute('SELECT BOQFile, FirstBOQRow, LastBOQRow, FirstExtRow, LastExtRow '
                                  'FROM boqbynes '
                                  'WHERE OSSNEName = ?', (mtuple[0], )).fetchall()
    lastpaperendrow = 0
    for papercut in boqpapercuts:
        wb1 = xl.Workbooks.Open(Filename=os.path.abspath(papercut[0]))
        if papercut[1] is not None:
            ws1 = wb1.Worksheets('SWAP')
            ws2.Cells(lastpaperendrow + 1, 1).Value = papercut[0].split('\\')[-1] + \
                                                      f'  [SWAP sheet] [Rows: {papercut[1]}...{papercut[2]}]'
            ws1.Range(f'C{papercut[1]}:G{papercut[2]}').Copy(ws2.Range(f'A{lastpaperendrow + 2}'))
            ws2.Rows(lastpaperendrow + 1).RowHeight = 40
            lastpaperendrow = lastpaperendrow + papercut[2] - papercut[1] + 2
        elif papercut[3] is not None:
            ws1 = wb1.Worksheets('Extension')
            ws2.Cells(lastpaperendrow + 1, 1).Value = papercut[0].split('\\')[-1] + \
                                                      f'  [Extension sheet] [Rows: {papercut[3]}...{papercut[4]}]'
            ws1.Range(f'C{papercut[3]}:G{papercut[4]}').Copy(ws2.Range(f'A{lastpaperendrow + 2}'))
            ws2.Rows(lastpaperendrow + 1).RowHeight = 40
            lastpaperendrow = lastpaperendrow + papercut[4] - papercut[3] + 2
        wb1.Close(SaveChanges=False)
    wb2.Close(SaveChanges=True)
    xl.Quit()

    # for i in [mtuple[4] - mtuple[3] + 3, mtuple[4] - mtuple[3] + 4]:
    #     sheet2.merge_cells('C' + str(i) + ':D' + str(i))
    #
    # for i in [1, 2]:
    #     sheet2.merge_cells('C' + str(i) + ':D' + str(i))

    #
    # for i in range(1, sheet2.max_row):
    #     if sheet2.cell(row=i, column=3).value in ['Main Equipment', 'Hardware License', 'Site Material', 'Hardware',
    #                                               'Auxiliary', 'Antennas', 'Antenna Auxiliary']:
    #         sheet2.merge_cells('C' + str(i) + ':D' + str(i))


def drawingidvk(targetfilepath, mtuple):
    global xl
    xl = win32.Dispatch("Excel.Application")
    xl.Visible = False
    wb1 = xl.Workbooks.Open(Filename=os.path.abspath(mtuple[1]))
    wb2 = xl.Workbooks.Open(Filename=targetfilepath)
    ws1 = wb1.Worksheets(1)
    ws1.Copy(Before=wb2.Worksheets(1))
    wb2.Close(SaveChanges=True)
    wb1.Close(SaveChanges=False)
    xl.Quit()


def drawingcpri(targetfilepath, mtuple):  # mtuple: NEName, IDVKFile, BOQFile, FirstBOQrow, LastBOQrow
    global book, con, cursor, finallyselectednename, mapold, mapnew, cprilist, upeua, oldbbutype, newbbutype,\
        rruremark, generrors, rrulib, generationmode
    upeua = False

    # Составление списка целевых наборов RRU
    # idvkrru ['RRUT', QTY]

    idvkrru = []
    book = openpyxl.open(targetfilepath, read_only=False)
    sheet = book.worksheets[0]
    i = 1
    for row in sheet.iter_rows():
        if row[1].value == 'Тип RRU':
            break
        i += 1
    for row in sheet.iter_rows(min_row=i + 1):
        if row[1].value is not None:
            if str.isdigit(row[1].value[3:6]):
                rruflag = False
                for rru in idvkrru:
                    if rru[0] == row[1].value[:7]:
                        rru[1] += 1
                        rruflag = True
                        break
                if not rruflag:
                    idvkrru.append([row[1].value[:7], 1])
        else:
            break

    # #Переход ко вкладке CPRI

    sheet = book['CPRI']
    freeslots = {0, 1, 2, 3, 4, 5, 6, 7, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28}
    freeports = {'0': [0, 'G'], '1': [0, 'G'], '2': [0, 'G'], '3': [0, 'G'],
                 '4': [0, 'G'], '5': [0, 'G'], '6': [0, 'G'], '7': [0, 'G']}
    oldbbutype = ''
    newbbutype = ''
    oldrrulist = []  # на случай новой БС, т.к. не будет добавлено старых RRU
    oldboards = ()
    newbbuqty = 0
    cprilist = []

    # OLD Boards installation

    try:
        newbbutype = cursor.execute("SELECT boqbyparts.PartName FROM boqbyparts "
                                    "WHERE boqbyparts.OSSNEName = ? "
                                    "AND boqbyparts.PartName like 'BBU____ Box'"
                                    # "AND boqbyparts.PartQty != '0'"  # коммент т.к. в BOQ бывает 0 BBU для новых БС
                                    , (finallyselectednename,)).fetchone()[0][:7]
        newbbuqty = cursor.execute("SELECT boqbyparts.PartQty FROM boqbyparts "
                                   "WHERE boqbyparts.OSSNEName = ? "
                                   "AND boqbyparts.PartName like 'BBU____ Box'"
                                   , (finallyselectednename,)).fetchone()[0]
    except:
        pass

    map3900new = {'0': 'E50',
                  '1': 'E51',
                  '2': 'E52',
                  '3': 'E53',
                  '4': 'F50',
                  '5': 'F51',
                  '6': 'F52',
                  '7': 'F53',
                  '18': 'G50',
                  '19': 'G52',
                  '20': 'G59',
                  '21': 'G60',
                  '22': 'G61',
                  '23': 'G62',
                  '24': 'G63',
                  '25': 'G64',
                  '26': 'G65',
                  '27': 'G66',
                  '28': 'G67',
                  '-1': 'E64'}
    map5900new = {'0': 'E50',
                  '1': 'F50',
                  '2': 'E51',
                  '3': 'F51',
                  '4': 'E52',
                  '5': 'F52',
                  '6': 'E53',
                  '7': 'F53',
                  '18': 'G50',
                  '19': 'G52',
                  '20': 'G59',
                  '21': 'G60',
                  '22': 'G61',
                  '23': 'G62',
                  '24': 'G63',
                  '25': 'G64',
                  '26': 'G65',
                  '27': 'G66',
                  '28': 'G67',
                  '-1': 'E64'}
    map3900old = {'0': 'A50',
                  '1': 'A51',
                  '2': 'A52',
                  '3': 'A53',
                  '4': 'B50',
                  '5': 'B51',
                  '6': 'B52',
                  '7': 'B53',
                  '18': 'C50',
                  '19': 'C52',
                  '-1': 'A64'}
    map5900old = {'0': 'A50',
                  '1': 'B50',
                  '2': 'A51',
                  '3': 'B51',
                  '4': 'A52',
                  '5': 'B52',
                  '6': 'A53',
                  '7': 'B53',
                  '18': 'C50',
                  '19': 'C52',
                  '-1': 'A64'}
    maprruold = {'0': 'A47',
                 '1': 'B46',
                 '2': 'C47',
                 '3': 'A56',
                 '4': 'B57',
                 '5': 'C56',
                 '-1': 'Z99'}
    maprrunew = {'0': 'E47',
                 '1': 'F46',
                 '2': 'G47',
                 '3': 'E56',
                 '4': 'F57',
                 '5': 'G56',
                 '-1': 'Z99'}

    # назначение маппингов БС по ячейкам

    if newbbutype[:7] == 'BBU5900':
        mapnew = map5900new
    else:
        mapnew = map3900new

    if oldbbutype == 'BBU5900':
        mapold = map5900old
    else:
        mapold = map3900old

    if generationmode == 'ext':
        oldbbutype = cursor.execute("SELECT BBUT FROM board WHERE NEName = ? AND BBUT != '' ",
                                    (mtuple[0],)).fetchone()[0]
        if newbbutype is None or newbbutype == '' or newbbuqty == 0:
            newbbutype = oldbbutype

        oldboards = cursor.execute("SELECT BRDT, Slot FROM board WHERE NEName = ? AND Subrack = 0 ",
                                   (mtuple[0],)).fetchall()
        sheet['C49'] = oldbbutype

        # установка старых плат

        if oldboards is not None:
            for board in oldboards:  # 'BRDT', 'Slot'
                if board[1] in ['1', '2', '3', '4', '5', '6', '7', '18', '19']:  # исключаем FAN и прочее
                    sheet[mapold[board[1]]].value = board[0]
        else:
            print(' No boards in old BBU. BS name: ' + mtuple[0])
            generrors.append(' No boards in old BBU. BS name: ' + mtuple[0])

        # начинается обработка старых RRU

        oldrru = cursor.execute("SELECT DISTINCT RRUT, RRUWM, HeadSlot FROM rru WHERE NEName = ? "
                                "GROUP BY RRUT", (mtuple[0],)).fetchall()

        # делаем oldrrulist ['old/dsm', 'RRUT(RRUWM)', HeadSlot, QTY] из oldrru ('RRUT', 'RRUWM', HeadSlot)

        oldrrulist = []
        i = 0
        for rru in oldrru:  # 'RRUT', 'RRUWM', HeadSlot, QTY
            if rru[0] is None:
                oldrrutype = 'RRU'
            else:
                oldrrutype = rru[0]
            oldrrulist.append(['old', ''.join([oldrrutype, '(', rru[1], ')']), rru[2]])
            oldrrulist[i].append(cursor.execute("SELECT COUNT (*) FROM rru WHERE NEName = ? AND RRUT = ?",
                                                (mtuple[0], rru[0])).fetchone()[0])
            i += 1

        #  проставление dsm на старые RRU

        for rru in oldrrulist:    # ['old/dsm', 'RRUT(RRUWM)', HeadSlot, QTY]
            rruflag = False
            for irru in idvkrru:
                if rru[1][:7] == irru[0]:
                    rruflag = True
                    break
            if not rruflag:  # no such old rru in idvk rru list
                rru[0] = 'dsm'

        #  установка и раскраска старых RRU на старой БС

        i = 0
        for rru in oldrrulist:  # ['old/dsm', 'RRUT(RRUWM)', HeadSlot, QTY]
            sheet[maprruold[str(i)]].value = str(rru[3]) + ' x ' + rru[1]
            cprilist.append([maprruold[str(i)], mapold[str(rru[2])]])
            if rru[0] == 'dsm':
                sheet[maprruold[str(i)]]._style = sheet['A62']._style  # покраска RRU под замену
            else:
                sheet[maprruold[str(i)]]._style = sheet['A60']._style  # покраска RRU под существующую
            i += 1

    # Done before: old RRU installed and colored
    #              old BRD installed
    #              oldboards:   BRDT, Slot
    #              oldrrulist:  # ['old/dsm', 'RRUT(RRUWM)', 'HeadSlot', QTY]

    # NEW Subrack side

    newboards = cursor.execute("SELECT PartName, PartQty FROM boqbyparts "
                               "WHERE OSSNEName = ? AND PartType = 'BRD' AND PartQty != 0",
                             (finallyselectednename,)).fetchall()
    newrrus = cursor.execute("SELECT PartName, PartQty FROM boqbyparts "
                             "WHERE OSSNEName = ? AND PartType = 'RRU' AND PartQty != 0",
                             (finallyselectednename,)).fetchall()

    # Заполнение перечня всех имеющихся плат (сначала новыми и только потом старыми)
    # boardlist ['old/new', 'PartName', oldslot]

    boardlist = []
    for newbrd in newboards:  # (PartName, QTY)
        for n in range(0, newbrd[1]):
            boardlist.append(['new', newbrd[0], -1])  # ['old/new', 'PartName', oldslot]
    for oldbrd in oldboards:
        boardlist.append(['old', oldbrd[0], int(oldbrd[1])])

    # Составление набора плат для новой BBU
    # finalboards ['old/new/dsm', 'PartName', newslot, oldslot]

    umptflag = False
    finalboards = []
    for partgroup in [['UMPT'], ['LMPT', 'WMPT', 'GTMU'], ['UBBP'], ['USCU'], ['LBBP'], ['WBBP'],
                         ['UPEU', 'UEIU', 'DCDU', 'Powe', 'EMUb', 'UBRI', 'UTRP', 'UCIU', 'UCCU']]:
        for item in boardlist:  # ['old/new', 'PartName', oldslot]
            lenfinal = len(finalboards)
            if item[1][:4] in partgroup:

                if newbbutype[:5] == 'BBU39':

                    if item[1][:4] in ['UMPT']:
                        if item[0] == 'new':
                            umptflag = True
                            freeslots.discard(7)
                            finalboards.append([item[0], item[1], 7, -1])
                        else:
                            if umptflag:
                                finalboards.append(['dsm', item[1], -1, item[2]])
                            else:
                                freeslots.discard(item[2])
                                finalboards.append([item[0], item[1], item[2], item[2]])

                    elif item[1][:4] in ['LMPT', 'WMPT', 'GTMU']:
                        for slot in [7, 6]:
                            if slot in freeslots and umptflag is not True:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            # print(' No free slots or no need ' + item[1] + '  Name: ' + mtuple[0])
                            # generrors.append(' No free slots or no need ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['UBBP']:
                        for slot in [2, 3, 1, 0, 4, 5]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['USCU']:
                        for slot in [4, 1, 0]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['LBBP']:
                        for slot in [2, 0, 1, 4]:
                            if slot in freeslots and umptflag is not True:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            # print(' No free slots or no need ' + item[1] + '  Name: ' + mtuple[0])
                            # generrors.append(' No free slots or no need ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['WBBP']:
                        for slot in [3, 0, 1, 4]:
                            if slot in freeslots and umptflag is not True:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            # print(' No free slots or no need ' + item[1] + '  Name: ' + mtuple[0])
                            # generrors.append(' No free slots or no need ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['UPEU']:
                        for slot in [19]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])

                    elif item[1][:4] in ['UEIU']:
                        for slot in [18]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['UBRI']:
                        for slot in [0, 1]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            # print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            # generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['UTRP']:
                        for slot in [0, 1, 4, 5]:
                            if slot in freeslots and umptflag is not True:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            # print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            # generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['DCDU', 'Powe', 'EMUb']:
                        for slot in [20, 21, 22, 23, 24, 25, 26, 27, 28]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    else:
                        finalboards.append(['dsm', item[1], -1, item[2]])
                        print(' Dismantled board for 3900 series: ' + item[1] + '  Name: ' + mtuple[0])
                        generrors.append(' Dismantled board for 3900 series: ' + item[1] + '  Name: ' + mtuple[0])

                elif newbbutype == 'BBU5900':

                    if item[1][:4] in ['UMPT']:
                        if item[0] == 'new':
                            umptflag = True
                            freeslots.discard(7)
                            finalboards.append([item[0], item[1], 7, -1])
                        else:
                            if umptflag:
                                finalboards.append(['dsm', item[1], -1, item[2]])
                            else:
                                freeslots.discard(item[2])
                                finalboards.append([item[0], item[1], item[2], item[2]])

                    elif item[1][:4] in ['UBBP']:
                        for slot in [4, 2, 0, 1, 3, 5]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['USCU']:
                        for slot in [4, 2, 0, 1, 3, 5]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['UPEU']:
                        for slot in [19]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])

                    elif item[1][:4] in ['UEIU']:
                        for slot in [18]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    elif item[1][:4] in ['DCDU', 'Powe', 'EMUb']:
                        for slot in [20, 21, 22, 23, 24, 25, 26, 27, 28]:
                            if slot in freeslots:
                                freeslots.discard(slot)
                                finalboards.append([item[0], item[1], slot, item[2]])
                                break
                        if lenfinal == len(finalboards):
                            finalboards.append(['dsm', item[1], -1, item[2]])
                            print(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])
                            generrors.append(' No free slots for ' + item[1] + '  Name: ' + mtuple[0])

                    else:
                        finalboards.append(['dsm', item[1], -1, item[2]])
                        # print(' Dismantled board for 5900 series: ' + item[1] + '  Name: ' + mtuple[0])
                        # generrors.append(' Dismantled board for 5900 series: ' + item[1] + '  Name: ' + mtuple[0])

    # finalboards is complete. ['old/new/dsm', 'PartName', newslot, oldslot]

    # Installation of all boards in new BBU
    # Coloring old bbu boards

    for item in finalboards:
        oldbbucell = sheet[mapold[str(item[3])]]
        newbbucell = sheet[mapnew[str(item[2])]]

        if item[0] == 'new':
            newbbucell.value = item[1]
            newbbucell._style = sheet['A61']._style  # покраска RRU под новое

        elif item[0] == 'old':
            newbbucell.value = item[1]
            if newbbutype == oldbbutype:
                if item[2] == item[3]:
                    newbbucell._style = sheet['A60']._style  # покраска RRU под существующее
                elif item[2] != item[3]:
                    newbbucell._style = sheet['A63']._style  # покраска RRU под перенос
                    oldbbucell._style = sheet['A63']._style  # покраска RRU под перенос

            if newbbutype != oldbbutype:
                newbbucell._style = sheet['A63']._style  # покраска RRU под перенос
                oldbbucell._style = sheet['A63']._style  # покраска RRU под перенос

        elif item[0] == 'dsm':
            oldbbucell._style = sheet['A62']._style  # покраска RRU под замену

    # Ячейки типов BBU

    sheet['G49'].value = newbbutype
    if newbbutype != oldbbutype:
        sheet['G49']._style = sheet['A61']._style  # покраска RRU под новое
        if oldbbutype != '':
            sheet['C49']._style = sheet['A62']._style  # покраска RRU под замену

    # Done before: OLD bbu RRU installed and colored
    #              OLD bbu BRD installed
    #              oldboards: BRDT, Slot
    #              oldrrulist:  # ['old/dsm', 'RRUT(RRUWM)', 'HeadSlot', QTY]
    #              Заполнение перечня всех имеющихся плат (сначала новыми и только потом старыми)
    #              boardlist ['old/new', 'PartName', oldslot]
    #              Составление набора плат для новой BBU
    #         +++  finalboards ['old/new/dsm', 'PartName', newslot, oldslot]
    #              NEW bbu BRD installed and colored
    #              OLD bbu BRD colored
    # Need to: NEW bbu RRU install and color
    #          Put OLD and NEW bbu CPRI lines
    #          Installing and dismantling list
    #
    #

    # freeports + upeu marker + box UPEU

    upeua = False
    for board in finalboards:  # ['old/new/dsm', 'PartName', newslot, oldslot]
        if board[0] != 'dsm':
            if board[1][:4] in ['UBBP', 'LBBP', 'WBBP', 'UBRI', 'GTMU']:
                freeports[str(board[2])][0] += 6
                freeports[str(board[2])][1] = board[1][0]
            if board[1] == 'UPEUA':
                upeua = True

    if newbbutype != oldbbutype:
        if newbbutype == 'BBU5900':
            upeua = False
            sheet[mapnew['19']].value = 'UPEUe'
            sheet[mapnew['19']]._style = sheet['A61']._style  # покраска RRU под новое
            if oldbbutype != '':
                sheet[mapold['19']]._style = sheet['A62']._style  # покраска RRU под замену
        elif newbbutype == 'BBU3910':
            upeua = False
            sheet[mapnew['19']].value = 'UPEUd'
            sheet[mapnew['19']]._style = sheet['A61']._style  # покраска RRU под новое
            if oldbbutype != '':
                sheet[mapold['19']]._style = sheet['A62']._style  # покраска RRU под замену

    # oldrrulist ['old/dsm', 'RRUT(RRUWM)', 'HeadSlot', QTY]
    # newrrus ['RRU', QTY] from BOQ
    # rrulib (справочник) ['PartNumber', 'PartName', 'WorkingMode'('U/L/W'), 'Priority for CPRI'(1..5)]
    # finalboards ['old/new/dsm', 'PartName', newslot, oldslot]
    # freeports {'slot': [qty, 'U/L/W']}

    finalrrus = []
    for rru in oldrrulist:
        if rru[0] != 'dsm':
            finalrrus.append([rru[0], rru[1], -1, rru[3]])   # ['old', 'RRUT(RRUWM)', -1, QTY]
    for rru in newrrus:
        finalrrus.append(['new', rru[0], -1, rru[1]])   # ['old/new', 'RRUT(RRUWM)', -1, QTY]

    for pri in [1, 2, 3, 4, 5]:
        for rru in finalrrus:
            if rru[2] == -1:
                for libitem in rrulib:   # rrulib (справочник) ['PartNumber', 'PartName', 'WorkingMode'('U/L/W'), Priority]
                    if rru[1][:7] == libitem[1][:7] and libitem[3] == pri:
                        if newbbutype == 'BBU5900':
                            slotorder = [4, 2, 0, 1, 3, 5]
                        else:
                            slotorder = [2, 3, 1, 0, 4, 5]
                        for slot in slotorder:
                            if freeports[str(slot)][1] == libitem[2] or freeports[str(slot)][1] == 'U':
                                if freeports[str(slot)][0] >= rru[3]:   # freeports {'slot': [qty, 'U/L/W']}
                                    freeports[str(slot)][0] -= rru[3]
                                    rru[2] = slot
                                    break
                        if rru[2] == -1:
                            for slot in slotorder:
                                if freeports[str(slot)][1] == 'U':
                                    if freeports[str(slot)][0] >= rru[3]:
                                        freeports[str(slot)][0] -= rru[3]
                                        rru[2] = slot
                                        break
                        break

    for rru in finalrrus:
        if rru[2] == -1:
            print(' No free CPRI ports for ' + rru[1] + ' BS Name: ' + mtuple[0])
            generrors.append(' No free CPRI ports for ' + rru[1] + ' BS Name: ' + mtuple[0])

    # finalrrus ['old/new', 'RRUT(RRUWM)', slot, QTY]
    # Need to: NEW bbu RRU install and color
    #          Put OLD and NEW bbu CPRI lines
    #          Installing and dismantling list
    #
    #

    rruremark = ''
    idvkrruamount = 0
    oldandboqamount = 0
    for rru in idvkrru:
        idvkrruamount += rru[1]
    for rru in finalrrus:
        oldandboqamount += rru[3]
    if idvkrruamount != oldandboqamount:
        rruremark = '[RRU mismatch]'

    # New parts table print on top

    newparts = cursor.execute("SELECT PartType, PartName, PartDescription, PartQty FROM boqbyparts "
                              "WHERE OSSNEName = ? AND PartType != '' AND PartQty != 0 LIMIT 14",
                              (mtuple[0],)).fetchall()
    i = 1
    sheet.merge_cells('D1:H1')
    for part in newparts:
        i += 1
        sheet.row_dimensions[i].hidden = False
        sheet.merge_cells('D' + str(i) + ':H' + str(i))
        sheet.cell(row=i, column=2).value = part[1]
        sheet.cell(row=i, column=3).value = part[3]
        sheet.cell(row=i, column=4).value = part[2]
    for board in finalboards:
        if board[0] == 'dsm':
            i += 1
            sheet.row_dimensions[i].hidden = False
            sheet.cell(row=i, column=1).value = board[1]
            sheet.cell(row=i, column=3).value = 1
    for rru in oldrrulist:
        if rru[0] == 'dsm':
            i += 1
            sheet.row_dimensions[i].hidden = False
            sheet.cell(row=i, column=1).value = rru[1]
            sheet.cell(row=i, column=3).value = rru[3]

    # Need to: NEW bbu RRU install and color
    #          Put OLD and NEW bbu CPRI lines
    # finalrrus ['old/new', 'RRUT(RRUWM)', slot, QTY]

    finalrrus.reverse()
    i = 0
    for rru in finalrrus:
        if i < 7:
            cprilist.append([maprrunew[str(i)], mapnew[str(rru[2])]])
            sheet[maprrunew[str(i)]].value = str(rru[3]) + ' x ' + rru[1]
            if rru[0] == 'new':
                sheet[maprrunew[str(i)]]._style = sheet['A61']._style  # покраска RRU как новую
            else:
                sheet[maprrunew[str(i)]]._style = sheet['A60']._style  # покраска RRU под существующую
            i += 1
        else:
            print(' More than 6 groups of RRU. BS Name: ' + mtuple[0])
            generrors.append(' More than 6 groups of RRU. BS Name: ' + mtuple[0])

    # Installation of all RRU in new BBU, finishing cprilist:
    # oldrrulist ['old/dsm', 'RRUT(RRUWM)', 'HeadSlot', QTY]


def drawingcprilines():
    global mapold, mapnew, cprilist, targetfileabs, finallyselectednename, xl
    xl.Quit()
    xl = win32.Dispatch("Excel.Application")
    xl.Visible = False
    wb = xl.Workbooks.Open(Filename=targetfileabs)
    ws = wb.Worksheets('CPRI')
    for cpri in cprilist:  # ['cell1', 'cell2']
        if cpri[0] == '-1' or cpri[1] == '-1':
            print(' CPRI list is not complete.  Name: ' + finallyselectednename)
            generrors.append(' CPRI list is not complete.  Name: ' + finallyselectednename)
        else:
            addline(ws, cpri[0], cpri[1])
    wb.Close(SaveChanges=True)
    xl.Quit()


def generateallidran():
    global finallyselectednename, generrors

    # Broken XLS files healing
    idvkxlslist = []
    for dirpath, dirnames, filenames in os.walk('.\\' + u2k + '\\IDVK\\' + form.comboBox.currentText()):
        for filename in filenames:
            if filename[-4:] in ['.xls', '.XLS']:
                idvkxlslist.append(os.path.abspath(str(os.path.join(dirpath, filename))))
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    for idvkxls in idvkxlslist:
        wb = excel.Workbooks.Open(idvkxls)
        wb.SaveAs(idvkxls[:-4] + '.xlsx', FileFormat=51)  # FileFormat = 51 is for .xlsx extension
        print(f' Resaving broken XLS to XLSX [{idvkxls}]', end="\r")
        wb.Close()
        os.remove(idvkxls)
    print(f'\n {len(idvkxlslist)} broken XLS fixed.\n')

    # Reload IDVK
    for file in listxlsxfilesinto(".\\" + u2k + "\\IDVK\\" + form.comboBox.currentText()):
        try:
            book = openpyxl.open(file, read_only=True)
        except:
            print(' File can\'t be opened: ' + file)
        else:
            sheet = book.active
            if cursor.execute("SELECT CustomerNEID FROM matching WHERE CustomerNEID = ?",
                              (sheet.cell(row=2, column=3).value,)).fetchone() is not None:
                cursor.execute("UPDATE matching SET IDVKFile = ?, IDVKNEName = ?, LongCustomerNEID = ? "
                               "WHERE CustomerNEID = ? AND MasterFlag > 0",
                               (file, sheet.cell(row=6, column=3).value, sheet.cell(row=4, column=3).value,
                                sheet.cell(row=2, column=3).value))
                print(' Matched: ', sheet.cell(row=2, column=3).value + '_' + sheet.cell(row=6, column=3).value)
            else:
                cursor.execute("INSERT INTO matching(OSSNEName, CustomerNEID, LongCustomerNEID, "
                               "IDVKFile, IDVKNEName, MasterFlag) "
                               "VALUES(?, ?, ?, ?, ?, ?)",
                               (sheet.cell(row=2, column=3).value + '_' + sheet.cell(row=6, column=3).value,
                                sheet.cell(row=2, column=3).value,
                                sheet.cell(row=4, column=3).value, file,
                                sheet.cell(row=6, column=3).value, 1))
                print(' Added: ', sheet.cell(row=2, column=3).value + '_' + sheet.cell(row=6, column=3).value)
            con.commit()
    con.execute("VACUUM")
    fillbssearchinglists()
    print('\n IDVK reload finished.\n')

    # IDRAN generation
    idvklist = listxlsxfilesinto('.\\' + u2k + '\\IDVK\\' + form.comboBox.currentText())
    gencounter = 0
    generrors = []
    for idvk in idvklist:
        b = openpyxl.open(idvk)
        s = b.active
        longid = s.cell(row=4, column=3).value
        b.close()
        finallyselectednename = cursor.execute("SELECT matching.OSSNEName FROM matching, boqbynes "
                                               "WHERE matching.IDVKFile != '' "
                                               "AND matching.MasterFlag > 0 "
                                               "AND matching.OSSNEName = boqbynes.OSSNEName "
                                               "AND boqbynes.BOQfile != '' "
                                               "AND matching.LongCustomerNEID = ?", (longid,)).fetchone()
        if finallyselectednename is not None:
            finallyselectednename = finallyselectednename[0]
            generateidran('')
            gencounter += 1
            print(' Done:   [', gencounter, '/', len(idvklist), ']')

        else:
            nequery = cursor.execute("SELECT "
                                     "OSSNEName, "
                                     "MasterFlag, "
                                     "CustomerNEID "
                                     "FROM "
                                     "matching "
                                     "WHERE "
                                     "LongCustomerNEID = ?", (longid,)).fetchone()
            try:
                nename = nequery[0]
            except:
                print('  No such BS by long ID.'
                      '  LongID: ' + longid +
                      '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])) + '                                       \n')
                generrors.append('  No such BS by long ID.'
                                 '  LongID: ' + longid +
                                 '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])))
            else:
                query = cursor.execute("SELECT "
                                       "matching.OSSNEName "
                                       "FROM "
                                       "matching, boqbynes "
                                       "WHERE "
                                       "matching.OSSNEName = boqbynes.OSSNEName "
                                       "AND matching.LongCustomerNEID = ?", (longid,)).fetchone()
                if query is None:
                    print('\n No such long ID in BOQ.'
                          '  ShortID: ' + nequery[2] +
                          '  LongID: ' + longid +
                          '  Name: ' + nename +
                          '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])) + '                                   \n')
                    generrors.append('  No such long ID in BOQ.'
                                     '  ShortID: ' + nequery[2] +
                                     '  LongID: ' + longid +
                                     '  Name: ' + nename +
                                     '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])))
                else:
                    query = cursor.execute("SELECT "
                                           "OSSNEName, "
                                           "CustomerNEID, "
                                           "LongCustomerNEID, "
                                           "MasterFlag, "
                                           "IDVKFile "
                                           "FROM "
                                           "matching "
                                           "WHERE "
                                           "LongCustomerNEID = ? "
                                           , (longid,)).fetchall()
                    if query[0][4] is None:
                        print('\n No IDVK info in database. BS: ' + query[0][0] +
                              '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])) + '\n')
                        generrors.append(' No IDVK info in database. BS: ' + query[0][0] +
                                         '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])))
                    else:  # Twin BBU or ID cross
                        print('\n 2 BBU on 1 site or ID cross.'
                              '  ShortID: ' + query[0][1] +
                              '  LongID: ' + longid +
                              '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])) + '                               \n')
                        generrors.append('  2 BBU on 1 site or ID cross.'
                                         '  ShortID: ' + query[0][1] +
                                         '  LongID: ' + longid +
                                         '  IDVK: ' + str('\\'.join(idvk.split('\\')[-2:])))
                        twinnumber = 1
                        boqbynetemplate = cursor.execute('SELECT * FROM boqbynes '
                                                         'WHERE OSSNEName = ?', (query[0][0],)).fetchone()
                        for item in query:
                            if twinnumber > 1:
                                if cursor.execute('SELECT OSSNEName FROM boqbynes '  # если ранее не добавлялось
                                                  'WHERE OSSNEName = ?', (item[0],)).fetchone() is None:
                                    cursor.execute('INSERT INTO boqbynes (OSSNEName, BOQFile, FirstBOQRow, LastBOQRow) '
                                                   'VALUES (?, ?, ?, ?)',
                                                   (item[0],
                                                    boqbynetemplate[1],
                                                    boqbynetemplate[2],
                                                    boqbynetemplate[3]))
                                    cursor.execute('COMMIT')
                            finallyselectednename = item[0]
                            generateidran('[TWIN ' + str(twinnumber) + ' ' + finallyselectednename + ']')
                            print(' [TWIN ' + str(twinnumber) + ' ' + finallyselectednename + '] is done             ')
                            twinnumber += 1

    errorsfilename = str('\\'.join(targetfileabs.split('\\')[:-1])) + '\\errors.txt'
    try:
        os.remove(errorsfilename)
    except:
        pass

    print(' Errors: [', len(generrors), ']\n'
          ' Folder:', form.comboBox.currentText(), '\n')

    if len(generrors) > 0:
        generrors.sort()
        print(' Errors happened:')
        for error in generrors:
            print(error)
        errorsfile = open(errorsfilename, 'w+')
        for error in generrors:
            errorsfile.write(error + '\n')
        errorsfile.close()

    print('\n  Operation complete.\n')


def singlegeneration():
    generateidran('')


def generateidran(twinmarker):
    global con, cursor, finallyselectednename, book, targetfileabs, generationmode, upeua, oldbbutype, newbbutype, \
        rruremark
    cursor.execute("UPDATE board SET BRDT = BoardName WHERE BRDT ISNULL")
    con.commit()
    if cursor.execute("SELECT NEName FROM subrack WHERE NEName = ? AND FrameType LIKE 'BBU%'",
                      (finallyselectednename, )).fetchone() is None:
        generationmode = 'ftk'
    else:
        generationmode = 'ext'
    try:
        newbbutype = cursor.execute("SELECT boqbyparts.PartName FROM boqbyparts "
                                    "WHERE boqbyparts.OSSNEName = ? "
                                    "AND boqbyparts.PartName like 'BBU____ Box'"
                                    # "AND boqbyparts.PartQty != '0'"  # коммент т.к. в BOQ бывает 0 BBU для новых БС
                                    , (finallyselectednename,)).fetchone()[0][:7]
    except:
        pass

    matchtuple = cursor.execute("SELECT matching.OSSNEName, matching.IDVKFile, boqbynes.BOQFile, boqbynes.FirstBOQRow, "
                                "boqbynes.LastBOQRow, matching.CustomerNEID, boqbynes.FirstExtRow, boqbynes.LastExtRow "
                                "FROM matching, boqbynes "
                                "WHERE matching.OSSNEName = boqbynes.OSSNEName AND matching.OSSNEName = ? ",
                                (finallyselectednename,)).fetchone()
    foldername = matchtuple[1].split('\\')[-2]
    if foldername == 'IDVK':  # no need to create extra folder in OUTPUTS
        targetfileabs = os.path.abspath('.\\' + u2k + '\\OUTPUTS\\' + matchtuple[5] +
                                        datetime.strftime(datetime.now(), "(%Y.%m.%d)") + '.xlsx')
    else:   # need to create extra folder in OUTPUTS
        if not os.path.exists('.\\' + u2k + '\\OUTPUTS\\' + foldername):
            os.mkdir('.\\' + u2k + '\\OUTPUTS\\' + foldername)
        targetfileabs = os.path.abspath('.\\' + u2k + '\\OUTPUTS\\' + foldername + '\\' + matchtuple[5] +
                                        datetime.strftime(datetime.now(), "(%Y.%m.%d)") + '.xlsx')
        try:
            os.remove(targetfileabs)
        except:
            pass
        shutil.copy('idrantemplate.xlsx', targetfileabs)

    if generationmode == 'ext':
        drawingidvk(targetfileabs, matchtuple)  # normally CLOSED win32 Close(SaveChanges=True) xl.Quit()
        print(strftime(" %H:%M:%S"), 'IDVK done. BOQ in progress...                                        ', end='\r')
        drawingboq(targetfileabs, matchtuple)
        print(strftime(" %H:%M:%S"), 'BOQ done, layout in progress...                                      ', end='\r')
        drawingcpri(targetfileabs, matchtuple)       # NOT CLOSED: book = openpyxl.open(targetfilepath, read_only=False)
        print(strftime(" %H:%M:%S"), 'layout done...                                                       ', end='\r')

    else:  # generationmode == 'new'
        if matchtuple is None:
            print(' BS', finallyselectednename, 'has no BOQ binding.')
        else:
            print(strftime(" %H:%M:%S"), 'BS:   ', matchtuple[0], 'is under operation...                   ', end='\r')
            # IDVK
            drawingidvk(targetfileabs, matchtuple)
            print(strftime(" %H:%M:%S"), ' IDVK done.                                                      ', end='\r')
            # BOQ
            drawingboq(targetfileabs, matchtuple)
            # CPRI
            drawingcpri(targetfileabs, matchtuple)
            book.save(targetfileabs + '1')
            book.close()
            os.remove(targetfileabs)
            os.rename(targetfileabs + '1', targetfileabs)
            print(strftime(" %H:%M:%S"), 'BOQ done.                                                        ', end='\r')

    temporaryfile = os.path.abspath('.\\' + u2k + '\\OUTPUTS\\' + matchtuple[0] + '(tmp).xlsx')
    # Scan Inventory files
    subrackfiles = []
    boardfiles = []
    for something in os.listdir('.\\' + u2k + '\\BRD'):
        if len(something.split('.')) > 1 and something.split('.')[-1] == 'csv':
            if something.split('_')[0] == 'Inventory':
                if something.split('_')[1] == 'Subrack':
                    subrackfiles.append(something)
                elif something.split('_')[1] == 'Board':
                    boardfiles.append(something)
    if len(subrackfiles) == 0:
        print('\n', strftime(" %H:%M:%S"),
              'No subrack inventory files found in "../BRD" folder. (Inventory_Subrack_*.csv)\n Operation failed.')
    if len(boardfiles) == 0:
        print('\n', strftime("%H:%M:%S"),
              'No board inventory files found in "../BRD" folder. (Inventory_Board_*.csv)\n Operation failed.')

    # Inventory (boards)
    for file in boardfiles:
        print(strftime(" %H:%M:%S"),
              'File "..BRD\\' + str(file) + '" is parsing...                                               ', end='\r')
        sheet = pandas.read_csv('.\\' + u2k + '\\BRD\\' + file,
                                header=0, dtype={2: "string", 22: "string", 20: "string", 23: "string", 24: "string",
                                                 25: "string", 28: "string", 26: "string"})
        sheet = sheet.loc[sheet['NEName'] == matchtuple[0]]
        try:
            os.remove(temporaryfile)
        except:
            pass
        if len(sheet) != 0:
            with pandas.ExcelWriter(temporaryfile) as writer:
                sheet.to_excel(writer, sheet_name='Inventory (boards)', engine='openpyxl')
            book1 = openpyxl.open(temporaryfile)
            sheet1 = book1.active
            sheet = book.create_sheet('Inventory (boards)')
            i = 0
            for row in sheet1.iter_rows():
                i += 1
                j = 0
                for cell in row:
                    j += 1
                    sheet.cell(row=i, column=j).value = cell.value
            sheet.delete_cols(1)
            dims = {}
            for row in sheet.rows:
                for cell in row:
                    if cell.value:
                        dims[cell.column] = max((dims.get(cell.column, 0), len(str(cell.value))))
            for col, value in dims.items():
                if col < 27:
                    sheet.column_dimensions[chr(64 + int(col))].width = value + 4
            book1.close()
            os.remove(temporaryfile)
            book.save(temporaryfile)
            book.close()
            os.remove(targetfileabs)
            os.rename(temporaryfile, targetfileabs)
            print(strftime(" %H:%M:%S"), 'Inventory (boards) done.                                         ', end='\r')

    # Inventory (subracks)
    for file in subrackfiles:
        print(strftime(" %H:%M:%S"),
              'File "..BRD\\' + str(file) + '" is parsing...                                               ', end='\r')
        sheet = pandas.read_csv('.\\' + u2k + '\\BRD\\' + file,
                                header=0, dtype={2: "string", 22: "string", 20: "string",
                                                 25: "string", 28: "string", 26: "string"})
        sheet = sheet.loc[sheet['NEName'] == matchtuple[0]]
        try:
            os.remove(temporaryfile)
        except:
            pass
        if len(sheet) != 0:
            with pandas.ExcelWriter(temporaryfile) as writer:
                sheet.to_excel(writer, sheet_name='Inventory (subracks)', engine='openpyxl')
            book1 = openpyxl.open(temporaryfile)
            sheet1 = book1.active
            sheet = book.create_sheet('Inventory (subracks)')
            i = 0
            for row in sheet1.iter_rows():
                i += 1
                j = 0
                for cell in row:
                    j += 1
                    sheet.cell(row=i, column=j).value = cell.value
            sheet.delete_cols(1)
            dims = {}
            for row in sheet.rows:
                for cell in row:
                    if cell.value:
                        dims[cell.column] = max((dims.get(cell.column, 0), len(str(cell.value))))
            for col, value in dims.items():
                if col < 27:
                    sheet.column_dimensions[chr(64 + int(col))].width = value + 4
            book1.close()
            os.remove(temporaryfile)
            # book.active = book[book.sheetnames[0]]
            book.save(temporaryfile)
            book.close()
            os.remove(targetfileabs)
            os.rename(temporaryfile, targetfileabs)
            print(strftime(" %H:%M:%S"), 'Inventory (subracks) done.                                       ', end='\r')

    print(strftime(" %H:%M:%S"), ' Drawing CPRI lines is going...                                          ', end='\r')
    drawingcprilines()
    print(strftime(" %H:%M:%S"), ' Drawing CPRI lines done.                                                ', end='\r')
    upeuaremark = ''
    bburemark = ''
    if oldbbutype == newbbutype:
        bburemark = '[R]'  # reuse current BBU
    elif oldbbutype == '':
        bburemark = '[N]'  # new BBU, inventory empty
    if upeua:
        upeuaremark = '[UPEUA]'  # UPEUA is in resulting BBU

    finalfilename = '\\'.join(targetfileabs.split('\\')[:-1]) + '\\' + twinmarker + 'ID_BS_' + matchtuple[5] + \
                    datetime.strftime(datetime.now(), "_(%Y.%m.%d)_RAN") + \
                    bburemark + upeuaremark + rruremark + '.xlsx'

    try:
        os.remove(finalfilename)
    except:
        pass
    os.rename(targetfileabs, finalfilename)
    print(strftime(" %H:%M:%S"), 'IDRAN:', matchtuple[0], 'complete.                                                 ')


def addline(ws, cell1, cell2):
    ltr = {'A': 1,
           'B': 2,
           'C': 3,
           'D': 4,
           'E': 5,
           'F': 6,
           'G': 7,
           'H': 8,
           'I': 9,
           'J': 10,
           'Z': 26}
    c1l = ltr[cell1[0]]
    c1t = int(cell1[1:])
    c2l = ltr[cell2[0]]
    c2t = int(cell2[1:])
    c1lft = ws.Cells(c1t, c1l).Left + (ws.Cells(c1t, c1l+1).Left - ws.Cells(c1t, c1l).Left)/2
    c1top = ws.Cells(c1t, c1l).Top + (ws.Cells(c1t+1, c1l).Top - ws.Cells(c1t, c1l).Top)/2
    c2lft = ws.Cells(c2t, c2l).Left + (ws.Cells(c2t, c2l+1).Left - ws.Cells(c2t, c2l).Left)/7
    c2top = ws.Cells(c2t, c2l).Top + (ws.Cells(c2t+1, c2l).Top - ws.Cells(c2t, c2l).Top)/2
    line = ws.Shapes.AddLine(c1lft, c1top, c2lft, c2top).Line


def sometest():
    pass


# form signals
form.pushButton_2.clicked.connect(generateallidran)
form.pushButton.clicked.connect(singlegeneration)
form.listView.clicked.connect(clickbslist)
form.tableView_4.clicked.connect(clickmatched)
form.tableView_3.clicked.connect(clicktotallymatched)
form.action_Run_raw_data_reimport.triggered.connect(clickrunbrdrrureimport)
form.action_Reload_matching_Customer_BSName_and_OSS_BSName.triggered.connect(clickimportmatching)
form.action_Auto_complete_matching_xlsx.triggered.connect(clickautocompletematching)
form.action_Reload_customer_s_long_BS_ID.triggered.connect(clickimportlongbsid)
form.action_Reconnect_database.triggered.connect(clickreconnectbsdb)
form.action_Reload_matching_IDVK.triggered.connect(clickreloadmatchingidvk)
form.action_XLS_to_XML.triggered.connect(clickxlstoxml)
form.action_Reload_new_equipment_from_BOQ.triggered.connect(clickboqreload)
form.action_Some_Test_Operation.triggered.connect(sometest)
form.action_Switch_to_11.triggered.connect(switchto11)
form.action_Switch_to_12.triggered.connect(switchto12)
form.action_Switch_to_13.triggered.connect(switchto13)
form.action_Switch_to_14.triggered.connect(switchto14)

app.exec()
